var assert = chai.assert;
var expect = chai.expect;

function resetLocalStorage(){
    localStorage.removeItem('labouchere');
    localStorage.removeItem('newlabouchere');
}


function showStatistic(history){
    var s = new Statistic(history);
        
    console.log('income:'+s.getIncome().toFixed(8));
    console.log('wager:'+s.getTotalWager().toFixed(8));
    console.log('win:'+s.getTotalWin().toFixed(8));
    console.log('lose:'+s.getTotalLose().toFixed(8));36
    console.log('counter win:'+s.getCounterWin());
    console.log('counter lose:'+s.getCounterLose());
    console.log('counter bet:'+s.getCounterBet());
    console.log('win percentage:'+s.getWinPercentage());
}

describe('Bot', function(){
    it('bet 1 time', function(){
        var bot = new Bot(new NewLabouchere(0.00000100, 0.00010000, true), new HighStrike(), new FreeBitCoin());
        bot._bettingStrategy.initBetList()
        assert.equal(bot._bettingStrategy.betList.length, 10);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 8);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 6);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 4);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 2);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length,0);




        assert.equal(bot._bettingStrategy.isDone(), true);

    });



    it('bet 2 times', function(){
        var bot = new Bot(new NewLabouchere(0.00000100, 0.00010000, true), new HighStrike(), new FreeBitCoin());
        bot._repeat = 2;
        assert.equal(bot._bettingStrategy.betList.length, 10);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 8);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 6);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 4);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length, 2);

        var betAmount = bot._bettingStrategy.getBetAmount();
        var betType = bot._decisionRule.bet();
        var isWin = true;
        bot._history.add({
            betAmount : betAmount,
            isWin : isWin,
            betType : betType,
            rollType : RollType.HIGH
        });
        bot._bettingStrategy.evaluateResult(isWin);
        assert.equal(bot._bettingStrategy.betList.length,0);
        bot._counterRepeat++;

        assert.equal(bot._bettingStrategy.isDone(), true);

        assert.equal(bot._counterRepeat < bot._repeat, true);

        bot._bettingStrategy.initBetList();
        assert.equal(bot._bettingStrategy.betList.length, 10);

    });
});

describe('DecisionRule', function(){
    it('alwaysHigh', function(){
        var rule = new HighStrike();
        
        assert.equal(Bet.HIGH, rule.bet());

    });
})

describe('History', function() {
        it('getLastRecord', function(){
            var h1 = new History();
            h1.add(Bet.HIGH);
            assert.equal(h1.getLastRecord(), Bet.HIGH);
            
        });
    
    
       it('should singleton', function() {
         var h1 = new History();
         h1.reset();
         h1.add(Bet.HIGH);
         h1.add(Bet.LOW);
         var h2 = new History();
         
           
         assert.equal(h1.getLastRecord(), h2.getLastRecord());
       });

        
        
});

describe('TrendRule', function(){
    it('test Trend', function(){
        var h = new History();
        h.reset();

        var trendRule = new TrendRule();

        //#1
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#2
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#3
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#4
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        
        trendRule.bet()

        //#5
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#6
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#7
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#8
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#9
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        
        trendRule.bet()

        //#10
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#11
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#12
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#13
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        // trendRule.bet()

        assert.equal(trendRule.bet(), HLTrend.HIGHSTRIKE);

        //#14
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        
        trendRule.bet()

        //#15
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        
        trendRule.bet()

        //#16
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        
        trendRule.bet()

        //#17
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        
        trendRule.bet()

        //#18
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        
        trendRule.bet()

        //#19
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        trendRule.bet();

        //#20
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        trendRule.bet();

        //#21
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        trendRule.bet();

        //#22
        h.add({
            betAmount : 0.00000001,
            isWin : false,
            betType : Bet.HIGH,
            rollType : RollType.LOW
        });
        trendRule.bet();

        //#23
        h.add({
            betAmount : 0.00000001,
            isWin : true,
            betType : Bet.HIGH,
            rollType : RollType.HIGH
        });
        

        assert.equal(trendRule.bet(), HLTrend.LOWSTRIKE);


        
        
        
    });
});


describe('Statistic', function(){
    it('bet statistic',  function(){
        var h = new History();
        h.reset();
        h.add({
            betAmount : 0.00000010,
            isWin : true
        });
        h.add({
            betAmount : 0.00000010,
            isWin : true
        });
        h.add({
            betAmount : 0.00000010,
            isWin : false
        });
        
        
        var s = new Statistic(h);
        
        
        assert.equal(s.getCounterBet(), 3);
        assert.equal(s.getCounterWin(), 2);
        assert.equal(s.getCounterLose(), 1);
        assert.equal(s.getTotalWager(), 0.00000030);
        assert.equal(s.getTotalWin(), 0.00000020);
        assert.equal(s.getTotalLose(), 0.00000010);
        assert.equal(s.getIncome(), 0.00000010);
    });


    it('trend no enough data', function(){
        var h = new History();
        h.reset();

        h.add({
            rollType : RollType.HIGH
        });

        
        var stat = new Statistic(h);
        assert.equal(stat.lastTrend(5), -1);
    });

    it('trend alternate low', function(){
        var h = new History();
        h.reset();

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.HIGH
        });
        
        var stat = new Statistic(h);
        // stat.lastTrend(5);
        assert.equal(stat.lastTrend(5), HLTrend.ALTLOW)

    });


    it('trend alternate high', function(){
        var h = new History();
        h.reset();

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.LOW
        });
        
        var stat = new Statistic(h);
        // stat.lastTrend(5);
        assert.equal(stat.lastTrend(5), HLTrend.ALTHIGH)

    });


    it('trend high strike', function(){
        var h = new History();
        h.reset();

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.HIGH
        });
        
        var stat = new Statistic(h);
        // stat.lastTrend(5);
        assert.equal(stat.lastTrend(5), HLTrend.HIGHSTRIKE)

    });

    it('trend low strike', function(){
        var h = new History();
        h.reset();

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.LOW
        });

        h.add({
            rollType : RollType.HIGH
        });

        h.add({
            rollType : RollType.LOW
        });
        
        var stat = new Statistic(h);
        // stat.lastTrend(5);
        assert.equal(stat.lastTrend(5), HLTrend.LOWSTRIKE)

    });
});





describe('NewLabouchere', function(){
    
    
    it('multiple betlist when split', function(){
        var targetWin = 0.00000100;
        var maxBet = 0.00000100;
        
        
        resetLocalStorage();
         var l = new NewLabouchere(targetWin, maxBet, true);
         assert.equal(l.maxBet , 0.00000100);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         
         assert.equal(l.maxBet , 0.00000200);
         
//          console.log('L : '+ JSON.parse(localStorage.getItem('labouchere')));
//         console.log("NL : "+JSON.parse(localStorage.getItem('newlabouchere')));
//         console.log('Bet : '+l.getBetAmount());
    });
    
    //list 1 : 70
    //list 2 : empty, loss then fill back list 2
    it('back to loss', function(){
        
        this.skip();
        var targetWin = 0.00000100;
        var maxBet = 0.00000100;
        
        
        resetLocalStorage();
         var l = new NewLabouchere(targetWin, maxBet);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(true);
         l.evaluateResult(true);
         l.evaluateResult(true);
         l.evaluateResult(true);
         
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         l.evaluateResult(false);
         
         l.evaluateResult(true);
         l.evaluateResult(true);
         l.evaluateResult(true);
         l.evaluateResult(true);
         l.evaluateResult(true);
         l.evaluateResult(false);
         l.evaluateResult(true);
         l.evaluateResult(false);
         l.evaluateResult(false);
//         l.evaluateResult(fals);
//         l.evaluateResult(false);
//         0.00000040,0.00000040
//        console.log('L : '+ JSON.parse(localStorage.getItem('labouchere')));
//         console.log("NL : "+JSON.parse(localStorage.getItem('newlabouchere')));
//         console.log('bet : '+l.getBetAmount());
         
//         l.evaluateResult(false);
//         console.log('L : '+ JSON.parse(localStorage.getItem('labouchere')));
//         console.log("NL : "+JSON.parse(localStorage.getItem('newlabouchere')));
//         console.log('Bet : '+l.getBetAmount());
         
//         l.evaluateResult(true);
//         l.evaluateResult(true);
//         l.evaluateResult(true);
//         l.evaluateResult(true);
//         l.evaluateResult(true);
//         l.evaluateResult(false);
         console.log('L : '+ JSON.parse(localStorage.getItem('labouchere')));
         console.log("NL : "+JSON.parse(localStorage.getItem('newlabouchere')));
         console.log('Bet : '+l.getBetAmount());
           
    });
    
    
    it('multiple split', function(){
        
//        this.skip();
        var targetWin = 0.00000100;
        var maxBet = 0.00000030;
        
        
        resetLocalStorage();
         var l = new NewLabouchere(targetWin, maxBet);
         // [10,10,10,10,10,10,10,10,10,10]
         assert.equal(l.getBetAmount(),0.00000020);
         
         
          l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
        l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6]
        assert.equal(l.getBetAmount(),0.00000009);
        
         l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6,9]
        assert.equal(l.getBetAmount(),0.00000012);
        
        l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6,9,12]
        assert.equal(l.getBetAmount(),0.00000015);
        
        l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6,9,12,15]
        assert.equal(l.getBetAmount(),0.00000018);
        
         l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6,9,12,15,18]
        assert.equal(l.getBetAmount(),0.00000021);
        
        
        l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6,9,12,15,18,21]
        assert.equal(l.getBetAmount(),0.00000024);
        
        l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
        assert.equal(l.getBetAmount(),0.00000027);
        
        
        l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3,3,3,3,3,3,3,3,3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
        
         l.evaluateResult(false);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3,3,3,3,3,3,3,3,3,3,6]
        assert.equal(l.getBetAmount(),0.00000009);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3,3,3,3,3,3,3,3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3,3,3,3,3,3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3,3,3,3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3,3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
//         [3]
        assert.equal(l.getBetAmount(),0.00000003);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,3,6,9,12,15,18,21,24]
        assert.equal(l.getBetAmount(),0.00000027);
        
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,3,6,9,12,15,18,21]
        assert.equal(l.getBetAmount(),0.00000024);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,3,6,9,12,15,18]
        assert.equal(l.getBetAmount(),0.00000021);
        
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,3,6,9,12,15]
        assert.equal(l.getBetAmount(),0.00000018);
        
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,3,6,9,12]
        assert.equal(l.getBetAmount(),0.00000015);
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,3,6,9]
        assert.equal(l.getBetAmount(),0.00000012);
        
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3,3,6]
        assert.equal(l.getBetAmount(),0.00000009);
        
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10]
//         [3,3]
        assert.equal(l.getBetAmount(),0.00000006);
        
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
         l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         [10]
        assert.equal(l.getBetAmount(),0.00000010);
        
        l.evaluateResult(true);
//        console.log(l.betList);
//         []
        assert.equal(l.getBetAmount(),0.00000000);
        
        
//        console.log('multiple split');
//        showStatistic(l.history);
        
    });
    
    it('several lose but not pass limit', function(){
        var targetWin = 0.00000100;
        var maxBet = 0.00000050;
        resetLocalStorage();
        var l = new NewLabouchere(targetWin, maxBet);
        // [10,10,10,10,10,10,10,10,10,10]
//        console.log(l.betList);
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        // [10,10,10,10,10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        // [10,10,10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(false);
//        console.log(l.betList);
        // [10,10,10,10,10,10,20]
        assert.equal(l.getBetAmount(),0.00000030);
        
        l.evaluateResult(false);
//        console.log(l.betList);
        // [10,10,10,10,10,10,20,30]
        assert.equal(l.getBetAmount(),0.00000040);
        
        
        l.evaluateResult(true);
//        console.log(l.betList);
        // [10,10,10,10,10,20]
        assert.equal(l.getBetAmount(),0.00000030);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        // [10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        // [10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        // []
        assert.equal(l.getBetAmount(),0.00000000);
        
//        console.log('several lose but not pass limit');
//        showStatistic(l.history);
        
    });
    
    it('alwayswin', function(){
        var targetWin = 0.00000100;
        var maxBet = 0.00000050;
        resetLocalStorage();

        var l = new NewLabouchere(targetWin, maxBet);
        
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
        assert.equal(l.getBetAmount(),0.00000020);
        
        assert.equal(l.isDone(), false)
        
        l.evaluateResult(true);
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(true);
        assert.equal(l.getBetAmount(),0);
        
        assert.equal(l.isDone(), true)
//        showStatistic(l.history);
        
        
        
        
    });
    
    
    it('evaluate with split', function(){
        resetLocalStorage();

        var l = new NewLabouchere(0.00000100, 0.00000050);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10]
        assert.equal(l.getBetAmount(),0.00000020);
        
        l.evaluateResult(false);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10,20]
        assert.equal(l.getBetAmount(),0.00000030);
        
        l.evaluateResult(false);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10,20,30]
        assert.equal(l.getBetAmount(),0.00000040);
        
        //start split bet list here 
        l.evaluateResult(false);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10,20,30,40]
        //[5,5,5,5,5,5,5,5,5,5]
        assert.equal(l.getBetAmount(),0.00000010);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10,20,30,40]
        //[5,5,5,5,5,5,5,5]
        assert.equal(l.getBetAmount(),0.00000010);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10,20,30,40]
        //[5,5,5,5,5,5]
        assert.equal(l.getBetAmount(),0.00000010);
        
        l.evaluateResult(true);
//        console.log(l.betList);
        //[10,10,10,10,10,10,10,10,10,10,20,30,40]
        //[5,5,5,5]
        assert.equal(l.getBetAmount(),0.00000010);
        
        l.evaluateResult(true);
//        console.log(l.betList);
         //[10,10,10,10,10,10,10,10,10,10,20,30,40]
        //[5,5]
        assert.equal(l.getBetAmount(),0.00000010);

      l.evaluateResult(true);
//      console.log(l.betList);
      //[10,10,10,10,10,10,10,10,10,20,30]
        //[]
      assert.equal(l.getBetAmount(),0.00000040);
      
      
      
      l.evaluateResult(true);
//      console.log(l.betList);
      //[10,10,10,10,10,10,10,10,20]
      assert.equal(l.getBetAmount(),0.00000030);
      
      l.evaluateResult(true);
//      console.log(l.betList);
      //[10,10,10,10,10,10,10]
      assert.equal(l.getBetAmount(),0.00000020);
      
      l.evaluateResult(true);
//      console.log(l.betList);
      //[10,10,10,10,10]
      assert.equal(l.getBetAmount(),0.00000020);
      
      l.evaluateResult(true);
//      console.log(l.betList);
      //[10,10,10]
      assert.equal(l.getBetAmount(),0.00000020);

      l.evaluateResult(true);
//      console.log(l.betList);
      //[10]
      assert.equal(l.getBetAmount(),0.00000010);

//      l.evaluateResult(true);
//      assert.equal(l.getBetAmount(),0.00000010);
      


      l.evaluateResult(true);
//      console.log(l.betList);
      //[]
      assert.equal(l.getBetAmount(),0);
//      console.log('evaluate with split');
//      showStatistic(l.history);
        
        
    });
});
        
describe('Labouchere', function(){
    it('should make bet list when initiate', function(){
       
        resetLocalStorage();
        
       var l = new Labouchere(500);
       assert.lengthOf(l.betList, 10);
       
      
      var arr = [];
      for(let i=0;i<10;i++){
          arr.push(parseFloat(50).toFixed(8));
      }
       
      
       assert.deepEqual(l.betList, arr);
       
       resetLocalStorage();
       var l2 = new Labouchere(0.00001000);
       assert.lengthOf(l2.betList, 10);
       
       
       var arr = [];
      for(let i=0;i<10;i++){
          arr.push(parseFloat(0.00000100).toFixed(8));
      }
       
       assert.deepEqual(l2.betList, arr);
    });
    
    
    
    it('getBetAmount+evaluate', function(){
        resetLocalStorage();
        var l = new Labouchere(0.00001000);
        
        assert.equal(l.getBetAmount(), 0.00000200);
        l.evaluateResult(true);
        assert.lengthOf(l.betList, 8);
        
        l.evaluateResult(true);
        assert.lengthOf(l.betList, 6);
        
        l.evaluateResult(true);
        assert.lengthOf(l.betList, 4);
        
        l.evaluateResult(true);
        assert.lengthOf(l.betList, 2);
        
        l.evaluateResult(false);
        assert.lengthOf(l.betList, 3);
        
        assert.equal(l.getBetAmount(), 0.00000300);
        
        l.evaluateResult(true);
        assert.lengthOf(l.betList, 1);
        
        assert.equal(l.getBetAmount(), 0.00000100);
        
        l.evaluateResult(true);
        assert.equal(l.getBetAmount(), 0);
        
    });
    
    
    
});