(function(){

const Bet = Object.freeze({
    HIGH:   1,
    LOW:  -1
});

const Result = Object.freeze({
    WIN:   1,
    LOSE:  0
});

const RollType = Object.freeze({
    HIGH : 1,
    FUCK : 0,
    LOW : -1
});

const HLTrend = Object.freeze({
    HIGHSTRIKE : 1,
    LOWSTRIKE : -1,
    ALTHIGH : 2,
    ALTLOW : -2
});

class Config{
    constructor(customEnum){
        this._intervalTimer = 1000;
        this._envMode;
        this._enum = customEnum;
    }
    
    get intervalTimer(){
        return 1000;
    }
    get envMode(){
        return this._enum.env.PROD;
    }
}
class Utils{
    static waitRandom(){
        return Math.floor((Math.random()*10)+1)*1000;
    }
}
class ArrayLS{
    constructor(id){
        this._arr = [];
        this._id = id;
        
        if(!this.isEmpty()){
            this.sync();
        }
    }
    push(value){
        this._arr.push(value);
        this.update();
    }
    get length(){
        return this._arr.length;
    }
    pop(){
        var ret = this._arr.pop();
        this.update();
        return ret;
    }
    shift(){
        var ret = this._arr.shift();
        this.update();
        return ret;
    }
    
    
    update(){
        localStorage.setItem(this._id, JSON.stringify(this._arr));
    }
    
    sync(){
        this._arr = JSON.parse(localStorage.getItem(this._id));
    }
    
    copy(){
        return this._arr;
    }
    
    assign(arr){
        this._arr = arr;
        this.update();
    }
    
    isEmpty(){


        return ((localStorage.getItem(this._id) == null) || (localStorage.getItem(this._id) == []) || (localStorage.getItem(this._id) == '[]') );
    }
    
    clear(){
        localStorage.removeItem(this._id);
    }
}
class History{
    constructor(){
       if(typeof History.instance === 'object'){
           return History.instance;
       }
      this._record = [];
       History.instance = this;
       return this;
    }
    reset(){
        this._record = [];
    }
    add(record){
      // if (record.hasOwnProperty('betAmount') && record.hasOwnProperty('isWin') && record.hasOwnProperty('betType') && record.hasOwnProperty('rollNumber'))
        this._record.push(record);
    }
    getLastRecord(){
        return this._record[this._record.length-1];
    }
    get record(){
        return this._record;
    }
    
}
class Statistic{
    constructor(){
        this._history = new History();
    }
    getCounterBet(){
        return this._history.record.length;
    }
    getTotalWager(){
        return this._history.record.reduce(function(total, item){
            return total+parseFloat(item.betAmount)
        }, 0);
       
    }

    lastTrend(n){
        var recLength = this.getCounterBet();
        if(recLength < n) return -1;

        var newArr = this._history.record.slice(recLength-n);


        var highStrikeCounter = 0;
        var lowStrikeCounter = 0;
        var altHighCounter = 0;
        var altLowCounter = 0;

        var currentTrend = null;
        var prevTrend = null;
        
        for (var i=1;i< newArr.length;i++){

            prevTrend = newArr[i-1].rollType;
            currentTrend = newArr[i].rollType;


            if(currentTrend == RollType.HIGH){
                if(prevTrend == RollType.HIGH){
                    highStrikeCounter++;
                }
                else
                if (prevTrend == RollType.LOW){
                    altLowCounter++;
                }
            }
            else
            if(currentTrend == RollType.LOW){
                if(prevTrend == RollType.HIGH){
                    altHighCounter++;
                }
                else
                if (prevTrend == RollType.LOW){
                    lowStrikeCounter++;
                }
            }

            
        }

        

        var ret = (highStrikeCounter >= lowStrikeCounter)?HLTrend.HIGHSTRIKE:HLTrend.LOWSTRIKE;
        var retValue = Math.max(highStrikeCounter, lowStrikeCounter);

        var ret2 = (altHighCounter >= altLowCounter)?HLTrend.ALTHIGH:HLTrend.ALTLOW;;
        var ret2Value = Math.max(altHighCounter, altLowCounter);

        

        var decision = (retValue >= ret2Value)?ret:ret2;


        return decision;

    }
    
    getCounterWin(){
        return this._history.record.filter(function(item){
            return item.isWin;
        }).length;
    }
    
    getTotalWin(){
        return this._history.record.filter(function(item){
            return item.isWin;
        }).reduce(function(total, item){
            return total+parseFloat(item.betAmount);
        },0);
    }
    
    getCounterLose(){
        return this._history.record.filter(function(item){
            return !item.isWin;
        }).length;
        
    }
    
    
    getTotalLose(){
        return this._history.record.filter(function(item){
            return !item.isWin;
        }).reduce(function(total, item){
            return total+parseFloat(item.betAmount);
        },0);
    }

    getWinPercentage(){
        return (this.getCounterWin()/this.getCounterBet())*100;
    }
    getIncome(){
        return this.getTotalWin()-this.getTotalLose();
    }

    show(){
        console.log('income:'+this.getIncome().toFixed(8));
        console.log('wager:'+this.getTotalWager().toFixed(8));
        console.log('win:'+this.getTotalWin().toFixed(8));
        console.log('lose:'+this.getTotalLose().toFixed(8));
        console.log('counter win:'+this.getCounterWin());
        console.log('counter lose:'+this.getCounterLose());
        console.log('counter bet:'+this.getCounterBet());
        console.log('win percentage:'+this.getWinPercentage());
    }
    

    
    
}
class ImplementHistory{
    constructor(){
        this._history = new History();
    }
}
class Labouchere{
    constructor(targetWin){
        this._targetWin = targetWin;
//        this._betList = [];
        
        // this._history = new History();
        this._betList = new ArrayLS("labouchere");
        this.initBetList();
        
    }
    initBetList(){
        var targetWin = this._targetWin;
        
        if(! this._betList.isEmpty())return;
        
        var itemList = parseFloat((targetWin/10).toFixed(8)).toFixed(8);
        for(let i=0;i<10;i++){
            this._betList.push(itemList);
        }
    }

    isDone(){
        return this._betList.length == 0;
    }
    getBetAmount(){
        var betList = this._betList.copy();
        
        
        if(betList.length == 0){
            return  0;
        }
        
        if(betList.length == 1){
            return  betList[0];
        }
            
        var firstElement = betList[0];
        var lastElement = betList[betList.length-1];
        var betAmount = parseFloat(firstElement) + parseFloat(lastElement);
        
        return parseFloat(betAmount).toFixed(8);
    }
    
    evaluateResult(isWin){

        
        var betAmount = this.getBetAmount();
        
        if(betAmount == 0){
            this._betList.clear();
            return;
        }
        
        
        // this._history.add({
        //     betAmount,
        //     isWin
        // });
        
        if(isWin){
            this._betList.pop();
            this._betList.shift();
        }
        else{
            this._betList.push(betAmount);
        }
        
        
    }
    
    get betList(){
        return this._betList.copy();
    }
    
    set betList(betList){
//        this._betList = betList;
        this._betList.assign(betList);
    }
    
    // get history(){
    //     return this._history;
    // }
}
class NewLabouchere extends Labouchere{
    constructor(){
        let targetWin, maxBet, increaseMaxBet;
        [targetWin = 0.00000010, maxBet=0.00001000, increaseMaxBet = false] = arguments;
        
        
        super(targetWin);
        this._maxBet = maxBet;
        this._tmpMaxBet = maxBet;
//        this._childBetList = [];
        this._childBetList = new ArrayLS('newlabouchere');
        this._increaseMaxBet = increaseMaxBet;
        
    }
    
    get maxBet(){
        return this._maxBet;
    }
    
    multipleMaxbet(){
        if(this._increaseMaxBet)this._maxBet += this._tmpMaxBet;
    }
    
    evaluateResult(isWin){
        super.evaluateResult(isWin);
        
        var betAmount = super.getBetAmount();
        
        if(betAmount >= this._maxBet){
//            console.log('amount : '+betAmount);
//            console.log('length : '+super.betList.length);
            
            if(super.betList.length > 2){
//                console.log('shift');
                super.betList.shift();
            }
//            console.log('pop');
            super.betList.pop();
            
            this._childBetList.push(super.betList);
            
            
            
            var item = parseFloat((betAmount/10).toFixed(8)).toFixed(8);
            var tmpList = [];
            
            for(let i=0;i<10;i++){
                tmpList.push(item);
            }
            
            this._childBetList.push(tmpList);
            
            super.betList = this._childBetList.pop();
            
            this.multipleMaxbet();
        }
        else if (betAmount == 0){
            if(this._childBetList.length > 0){
                super.betList = this._childBetList.pop();
            }
        }
        

        
        
        
        
    }
}
class SameAmount{
    constructor(){
        this._betAmount = '0.00000002';
    }
    get betAmount(){
        return this._betAmount;
    }
    
}
class FreeBitCoin{
    constructor(){
        
        this._intervalTimer = 1000;
        
        this._balance = 0;
        this._betAmount = 0;
        
        this._lastRollNumber = 0;
        this._maxLow = 4750;
        this._minHigh = 5250;
        
    }
    sync(){

        this._balance = $('#balance').html();
    }
    getBalance(){
        return this._balance;
    }
    setBetAmount(amount){
        this._betAmount = amount;
        $('#double_your_btc_stake').val(amount);
        var self = this;
        
        var promise = new Promise(function(resolve, reject){
         var timer = setInterval(function(){
             
           if($('#double_your_btc_stake').val() == amount){
             clearInterval(timer);
             return resolve();
           }

         },self._intervalTimer);
       });
       
       return promise;
        
    }
    
    
    bet(betType){
        
        var action = (betType == Bet.HIGH)?"$('#double_your_btc_bet_hi_button').click()":"$('#double_your_btc_bet_lo_button').click()";
        var self = this;
        eval(action);
        var promise = new Promise(function(resolve, reject){
            
                
                self.rolling().then(function(isDone){
                    if(isDone){
                        var rollNumber = self.getRollNumber();
                        var ret = RollType.FUCK;


                        // if((betType == Bet.HIGH)&&(rollNumber > self._minHigh))ret = true;    
                        if (rollNumber > self._minHigh)ret = RollType.HIGH;
                        // if((betType == Bet.LOW)&&(rollNumber < self._maxLow))ret = true;
                        if (rollNumber < self._maxLow)ret = RollType.LOW;
                        
                        return resolve(ret);
                    }
                });
                
                
                
            
        });
        
        return promise;
    }
//    betHigh(){
//        return this.bet("$('#double_your_btc_bet_hi_button').click()");
//    }
//    betLow(){
//        return this.bet("$('#double_your_btc_bet_lo_button').click()");
//    }
    
    getRollNumber(){
        var first = document.getElementById('multiplier_first_digit').innerHTML;
        var second = document.getElementById('multiplier_second_digit').innerHTML;
        var third = document.getElementById('multiplier_third_digit').innerHTML;
        var fourth = document.getElementById('multiplier_fourth_digit').innerHTML;
        var fifth = document.getElementById('multiplier_fifth_digit').innerHTML;
        var rollNumber = first+second+third+fourth+fifth;
        
        return rollNumber;
    }
    
    get lastRollNumber(){
        return this._lastRollNumber;
    }
    
    rolling(){
        var self = this;
        var promise =  new Promise(function(resolve, reject){
            
            
            var timer = setInterval(function(){
                var oldNumber = self.getRollNumber();

                setTimeout(function(){
                    var newNumber = self.getRollNumber();
                    if (oldNumber == newNumber){
                        clearInterval(timer);
                        return resolve(oldNumber == newNumber);
                    }
                }, 500);
            }, self._intervalTimer);
        });
        return promise;
    }
}
class FreeBitCoinDev{
    constructor(){
        this._balance = 0;
        this._betAmount = 0;
        
    }
    sync(){
//        this._balance = parseFloat($('#balance').html());
        //this._balance = parseFloat(0.0002);
        this._balance = $('#balance').html();
    }
    getBalance(){
        return this._balance;
    }
    setBetAmount(){
        
    }
    bet(action){
        var self = this;
        var isClick = false;
        var promise = new Promise(function(resolve, reject){
            var timer = setInterval(function(){
                
                    
                if(!isClick){
                    eval(action);
                    isClick = true;
                }
                self.rolling().then(function(isDone){
                    if(isDone){
                        clearInterval(timer);
                        return resolve(true);
                    }
                });
                
                
                
            }, INTERVALTIMER);
        });
        
        return promise;
    }
    betHigh(){
        return this.bet("$('#double_your_btc_bet_hi_button').click()");
    }
    betLow(){
        return this.bet("$('#double_your_btc_bet_lo_button').click()");
    }
    
}
class DecisionRule{
    constructor(){

    }
    bet(){
        
    }

}
class AlternateHigh{
    constructor(){
        this._counter = 0;
    }
    bet(){
        var ret = (this._counter % 2 == 0)?Bet.HIGH:Bet.LOW;
        this._counter++;
        return ret;
    }

}
class AlternateLow{
    constructor(){
        this._counter = 0;
    }
    bet(){
        var ret = (this._counter % 2 == 0)?Bet.LOW:Bet.HIGH;
        this._counter++;
        return ret;
    }

}
class HighStrike{
    constructor(){
        
    }
    bet(){
        return Bet.HIGH;
    }

}
class LowStrike{
    constructor(){
        
    }
    bet(){
        return Bet.LOW;
    }

}
class RandomRule{
    constructor(){
        
    }
    bet(){
        var randomNumber = Math.floor((Math.random() * 100));
        return (randomNumber <= 50)?Bet.LOW:Bet.HIGH;
    }

}
class RuleFactory{
    constructor(){
        
    }
    makeRule(type){

        var ruleClass = null;
        if(type == HLTrend.HIGHSTRIKE) ruleClass = HighStrike;
        if(type == HLTrend.LOWSTRIKE) ruleClass = LowStrike;
        if(type == HLTrend.ALTHIGH) ruleClass = AlternateHigh;
        if(type == HLTrend.ALTLOW) ruleClass = AlternateLow;
        if(type == 'random') ruleClass = RandomRule;

        return new ruleClass();
    }
}
class TrendRule{
    constructor(){
        this._statistic = new Statistic();
        this._counter = 0;
        this._ruleFactory = new RuleFactory();
        this._trend = this._ruleFactory.makeRule('random');
    }
    bet(){
        this._counter++;
        
        if (this._counter % 13 != 0) return this._trend.bet();

        var longTrend = this._statistic.lastTrend(13);
        var shortTrend = this._statistic.lastTrend(5);

        if(longTrend == shortTrend) this._trend =  this._ruleFactory.makeRule(longTrend);
        if(longTrend != shortTrend) this._trend = this._ruleFactory.makeRule('random');

        this._counter -= 5;
        return this._trend.bet();
        
    }

}

class Bot{
    constructor(bettingStrategy, decisionRule, bettingSite){
        this._bettingStrategy = bettingStrategy;
        this._decisionRule = decisionRule;
        this._bettingSite = bettingSite;
        this._timer = null;
        this._onProcess = false;
        this._history = new History();
        this._statistic = new Statistic(this._history);
        this._counterRepeat = 0;
        this._repeat = 1;
    }
    
    init(){

    }
    
    start(){
        if(arguments.length == 1){
            this._repeat = arguments[0];
        }


        var self = this;

        this._bettingStrategy.initBetList();
        this._timer = setInterval(function(){

            
            if(self._onProcess)return;
            var betAmount = self._bettingStrategy.getBetAmount();
            
            self._onProcess = true;
            
            self._bettingSite.setBetAmount(betAmount).then(function(){
                var betType = self._decisionRule.bet();
                self._bettingSite.bet(betType).then(function(result){
                    var isWin = false;
                    if (betType == Bet.HIGH && result == RollType.HIGH) isWin = true;
                    if (betType == Bet.LOW && result == RollType.LOW) isWin = true;
                    

                    self._history.add({
                        betAmount : betAmount,
                        isWin : isWin,
                        betType : betType,
                        rollType : result
                    });

                    self._bettingStrategy.evaluateResult(isWin);
                    self._onProcess = false;
                    if(self._bettingStrategy.isDone()){
                        self._counterRepeat++;
                        self.stop();
                        self._statistic.show();
                        self._history.reset();
                    }
                });
                
            })
        },5000);
        
            
        
    }
    stop(){
        clearInterval(this._timer);
        if(this._counterRepeat < this._repeat){
            this.start();
        }
    }
}
var bot = new Bot(new NewLabouchere(0.00000010, 0.00001000, true), new TrendRule(), new FreeBitCoin());

setTimeout(function(){
    console.log('bot betting start');
    bot.start(10);
}, 2000);




})();