/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function (grunt) {
    // Project configuration.
    
    var myFile = [
        'client/CustomEnum.js',
        'client/Config.js',
        'client/Utils.js',
        'client/ArrayLS.js',
        'client/History.js',
        'client/Statistic.js',
        'client/bettingStrategy/*.js',
        'client/bettingSite/*.js',
        'client/DecisionRule/DecisionRule.js',
        'client/DecisionRule/*.js',
        'client/AOP/*.js',
        'client/Bot.js',
        'client/Main.js'
    ];
    
    grunt.initConfig({
        concat : {
            dist : {
                src : myFile,
                dest : 'dewajudi.js'
            }
        },
        
        watch : {
            configFiles: {
                files: [ 
                    'test/test.js',
                    'dewajudi.js'
                ],
                options: {
                  livereload: true,
                }
            },
            concat : {
                files : myFile,
                tasks : 'concat'
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-concat');
   
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', 'watch');
    
};
