class Statistic{
    constructor(){
        this._history = new History();
    }
    getCounterBet(){
        return this._history.record.length;
    }
    getTotalWager(){
        return this._history.record.reduce(function(total, item){
            return total+parseFloat(item.betAmount)
        }, 0);
       
    }

    lastTrend(n){
        var recLength = this.getCounterBet();
        if(recLength < n) return -1;

        var newArr = this._history.record.slice(recLength-n);


        var highStrikeCounter = 0;
        var lowStrikeCounter = 0;
        var altHighCounter = 0;
        var altLowCounter = 0;

        var currentTrend = null;
        var prevTrend = null;
        
        for (var i=1;i< newArr.length;i++){

            prevTrend = newArr[i-1].rollType;
            currentTrend = newArr[i].rollType;


            if(currentTrend == RollType.HIGH){
                if(prevTrend == RollType.HIGH){
                    highStrikeCounter++;
                }
                else
                if (prevTrend == RollType.LOW){
                    altLowCounter++;
                }
            }
            else
            if(currentTrend == RollType.LOW){
                if(prevTrend == RollType.HIGH){
                    altHighCounter++;
                }
                else
                if (prevTrend == RollType.LOW){
                    lowStrikeCounter++;
                }
            }

            
        }

        

        var ret = (highStrikeCounter >= lowStrikeCounter)?HLTrend.HIGHSTRIKE:HLTrend.LOWSTRIKE;
        var retValue = Math.max(highStrikeCounter, lowStrikeCounter);

        var ret2 = (altHighCounter >= altLowCounter)?HLTrend.ALTHIGH:HLTrend.ALTLOW;;
        var ret2Value = Math.max(altHighCounter, altLowCounter);

        

        var decision = (retValue >= ret2Value)?ret:ret2;


        return decision;

    }
    
    getCounterWin(){
        return this._history.record.filter(function(item){
            return item.isWin;
        }).length;
    }
    
    getTotalWin(){
        return this._history.record.filter(function(item){
            return item.isWin;
        }).reduce(function(total, item){
            return total+parseFloat(item.betAmount);
        },0);
    }
    
    getCounterLose(){
        return this._history.record.filter(function(item){
            return !item.isWin;
        }).length;
        
    }
    
    
    getTotalLose(){
        return this._history.record.filter(function(item){
            return !item.isWin;
        }).reduce(function(total, item){
            return total+parseFloat(item.betAmount);
        },0);
    }

    getWinPercentage(){
        return (this.getCounterWin()/this.getCounterBet())*100;
    }
    getIncome(){
        return this.getTotalWin()-this.getTotalLose();
    }

    show(){
        console.log('income:'+this.getIncome().toFixed(8));
        console.log('wager:'+this.getTotalWager().toFixed(8));
        console.log('win:'+this.getTotalWin().toFixed(8));
        console.log('lose:'+this.getTotalLose().toFixed(8));
        console.log('counter win:'+this.getCounterWin());
        console.log('counter lose:'+this.getCounterLose());
        console.log('counter bet:'+this.getCounterBet());
        console.log('win percentage:'+this.getWinPercentage());
    }
    

    
    
}