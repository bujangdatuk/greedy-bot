class AlternateHigh{
	constructor(){
		this._counter = 0;
	}
	bet(){
		var ret = (this._counter % 2 == 0)?Bet.HIGH:Bet.LOW;
		this._counter++;
		return ret;
	}

}