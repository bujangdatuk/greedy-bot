class RuleFactory{
	constructor(){
		
	}
	makeRule(type){

		var ruleClass = null;
		if(type == HLTrend.HIGHSTRIKE) ruleClass = HighStrike;
		if(type == HLTrend.LOWSTRIKE) ruleClass = LowStrike;
		if(type == HLTrend.ALTHIGH) ruleClass = AlternateHigh;
		if(type == HLTrend.ALTLOW) ruleClass = AlternateLow;
		if(type == 'random') ruleClass = RandomRule;

		return new ruleClass();
	}
}