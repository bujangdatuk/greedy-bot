class AlternateLow{
	constructor(){
		this._counter = 0;
	}
	bet(){
		var ret = (this._counter % 2 == 0)?Bet.LOW:Bet.HIGH;
		this._counter++;
		return ret;
	}

}