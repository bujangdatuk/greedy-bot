class TrendRule{
	constructor(){
		this._statistic = new Statistic();
		this._counter = 0;
		this._ruleFactory = new RuleFactory();
		this._trend = this._ruleFactory.makeRule('random');
	}
	bet(){
		this._counter++;
		
		if (this._counter % 13 != 0) return this._trend.bet();

		var longTrend = this._statistic.lastTrend(13);
		var shortTrend = this._statistic.lastTrend(5);

		if(longTrend == shortTrend) this._trend =  this._ruleFactory.makeRule(longTrend);
		if(longTrend != shortTrend) this._trend = this._ruleFactory.makeRule('random');

		this._counter -= 5;
		return this._trend.bet();
		
	}

}