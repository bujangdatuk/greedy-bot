class Config{
    constructor(customEnum){
        this._intervalTimer = 1000;
        this._envMode;
        this._enum = customEnum;
    }
    
    get intervalTimer(){
        return 1000;
    }
    get envMode(){
        return this._enum.env.PROD;
    }
}