class Trend{
    constructor(){
        const TRENDTYPE = {
            HIGH_STRIKE,
            LOW_STRIKE,
            ALTERNATE_HIGH_START,
            ALTERNATE_LOW_START
        }
    }
    
    static get TRENDTYPE(){
        return TRENDTYPE;
    }
}