const Bet = Object.freeze({
    HIGH:   1,
    LOW:  -1
});

const Result = Object.freeze({
    WIN:   1,
    LOSE:  0
});

const RollType = Object.freeze({
	HIGH : 1,
	FUCK : 0,
	LOW : -1
});

const HLTrend = Object.freeze({
	HIGHSTRIKE : 1,
	LOWSTRIKE : -1,
	ALTHIGH : 2,
	ALTLOW : -2
});
