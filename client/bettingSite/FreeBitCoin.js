class FreeBitCoin{
    constructor(){
        
        this._intervalTimer = 1000;
        
        this._balance = 0;
        this._betAmount = 0;
        
        this._lastRollNumber = 0;
        this._maxLow = 4750;
        this._minHigh = 5250;
        
    }
    // sync(){

    //     this._balance = $('#balance').html();
    // }
    getBalance(){
        var totalBalance = 0;
        totalBalance = parseFloat($('#balance').html());
        if (document.getElementById('bonus_account_balance') != null) totalBalance += parseFloat($('#bonus_account_balance').html());
        
        return totalBalance;
    }

    getRefreshTimeRemaining(){
        return $('#time_remaining .countdown_amount').eq(0).html();
    }

    setBetAmount(amount){
        this._betAmount = amount;
        $('#double_your_btc_stake').val(amount);
        var self = this;
        
        var promise = new Promise(function(resolve, reject){
         var timer = setInterval(function(){
             
           if($('#double_your_btc_stake').val() == amount){
             clearInterval(timer);
             return resolve();
           }

         },self._intervalTimer);
       });
       
       return promise;
        
    }
    
    
    bet(betType){
        
        var action = (betType == Bet.HIGH)?"$('#double_your_btc_bet_hi_button').click()":"$('#double_your_btc_bet_lo_button').click()";
        var self = this;
        eval(action);
        var promise = new Promise(function(resolve, reject){
            
                
                self.rolling().then(function(isDone){
                    if(isDone){
                        var rollNumber = self.getRollNumber();
                        var ret = RollType.FUCK;


                        // if((betType == Bet.HIGH)&&(rollNumber > self._minHigh))ret = true;    
                        if (rollNumber > self._minHigh)ret = RollType.HIGH;
                        // if((betType == Bet.LOW)&&(rollNumber < self._maxLow))ret = true;
                        if (rollNumber < self._maxLow)ret = RollType.LOW;
                        
                        return resolve(ret);
                    }
                });
                
                
                
            
        });
        
        return promise;
    }
//    betHigh(){
//        return this.bet("$('#double_your_btc_bet_hi_button').click()");
//    }
//    betLow(){
//        return this.bet("$('#double_your_btc_bet_lo_button').click()");
//    }
    
    getRollNumber(){
        var first = document.getElementById('multiplier_first_digit').innerHTML;
        var second = document.getElementById('multiplier_second_digit').innerHTML;
        var third = document.getElementById('multiplier_third_digit').innerHTML;
        var fourth = document.getElementById('multiplier_fourth_digit').innerHTML;
        var fifth = document.getElementById('multiplier_fifth_digit').innerHTML;
        var rollNumber = first+second+third+fourth+fifth;
        
        return rollNumber;
    }
    
    get lastRollNumber(){
        return this._lastRollNumber;
    }
    
    rolling(){
        var self = this;
        var promise =  new Promise(function(resolve, reject){
            
            
            var timer = setInterval(function(){
                var oldNumber = self.getRollNumber();

                setTimeout(function(){
                    var newNumber = self.getRollNumber();
                    if (oldNumber == newNumber){
                        clearInterval(timer);
                        return resolve(oldNumber == newNumber);
                    }
                }, 500);
            }, self._intervalTimer);
        });
        return promise;
    }
}