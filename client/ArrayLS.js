class ArrayLS{
    constructor(id){
        this._arr = [];
        this._id = id;
        
        if(!this.isEmpty()){
            this.sync();
        }
    }
    push(value){
        this._arr.push(value);
        this.update();
    }
    get length(){
        return this._arr.length;
    }
    pop(){
        var ret = this._arr.pop();
        this.update();
        return ret;
    }
    shift(){
        var ret = this._arr.shift();
        this.update();
        return ret;
    }
    
    
    update(){
        localStorage.setItem(this._id, JSON.stringify(this._arr));
    }
    
    sync(){
        this._arr = JSON.parse(localStorage.getItem(this._id));
    }
    
    copy(){
        return this._arr;
    }
    
    assign(arr){
        this._arr = arr;
        this.update();
    }
    
    isEmpty(){


        return ((localStorage.getItem(this._id) == null) || (localStorage.getItem(this._id) == []) || (localStorage.getItem(this._id) == '[]') );
    }
    
    clear(){
        localStorage.removeItem(this._id);
    }
}