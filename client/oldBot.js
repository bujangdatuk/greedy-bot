(function(){
  

var Helper = (function(){
  var showLog = false;
  return{
    waitRandom : function(){
        var promise = new Promise(function(resolve, reject){
        var waitTime = Math.floor((Math.random() * 10000) + 1000);
        if(showLog)console.log('wait time : '+waitTime);
        setTimeout(function(){
          return resolve();
        }, waitTime);

      });
      return promise;
    },
    setLog : function(status){
      showLog = status;
    }
  }
})();

var Freebitcoin = (function(){
        
//  var balance = 0;
//  var countdownRoll = {
//    minute : null,
//    second : null
//  };
//  var timer = null;
//  
  //reward
  //var status
  
  
  return{
//    sync : function(){
//      timer = setInterval(function(){
//        balance = $('#balance').html();
//        countdownRoll.minute = $('#time_remaining .countdown_amount').eq(0).html();
//        countdownRoll.second = $('#time_remaining .countdown_amount').eq(1).html();
//      }, 1000);
//    },
//    pause : function(){
//      clearInterval(timer);
//    },
    getBalance : function(){
//                    var balancebonus = parseFloat($('#bonus_account_balance').html().replace(' BTC',''));
                    var balance = parseFloat($('#balance').html());
//                    return balance+balancebonus;
                    return balance;
    },
    getCountDownRoll : function(){
                    return $('#time_remaining .countdown_amount').eq(0).html();
    },
                getBetRollNumber : function(){
                    var digit1 = $('#multiplier_first_digit').html();
                    var digit2 = $('#multiplier_second_digit').html();
                    var digit3 = $('#multiplier_third_digit').html();
                    var digit4 = $('#multiplier_fourth_digit').html();
                    var digit5 = $('#multiplier_fifth_digit').html();
                    
                    var rollNumber = digit1+digit2+digit3+digit4+digit5;
                    return rollNumber;
                },
                getFreeRollNumber : function(){
                    var digit1 = $('#free_play_first_digit').html();
                    var digit2 = $('#free_play_second_digit').html();
                    var digit3 = $('#free_play_third_digit').html();
                    var digit4 = $('#free_play_fourth_digit').html();
                    var digit5 = $('#free_play_fifth_digit').html();
                    
                    var rollNumber = digit1+digit2+digit3+digit4+digit5;
                    return rollNumber;
                },
                getRollWinning : function(){
                    return $("#winnings").html();
                },
                getRollRPWinning : function(){
                    return $("#fp_reward_points_won").html();
                },
                getRewardPoint : function(){
                    return $( "div:contains('YOUR REWARD POINTS (RP)')" ).last().siblings().html().replace(/,/g, '');
                },
                getBonusIncreasePercent : function(){
                    return $("#fp_bonus_req_completed").html();
                },
                getMinReward : function(){
                    var minReward = $("#fp_min_reward").html();
//                    return minReward == undefined ? "0": minReward.match(/(0\.)\w+/g)[0];
                    return minReward == undefined ? "0": minReward.replace(' BTC','');
                },
                getBonusBalance : function(){
                    var bonusAccountBalance = $("#bonus_account_balance").html();
//                    return bonusAccountBalance == undefined ? "0" : bonusAccountBalance.match(/(0\.)\w+/g)[0];
                    return bonusAccountBalance == undefined ? "0" : bonusAccountBalance.replace(' BTC','');
                },
                getBonusWagerRemaining : function(){
                    var bonusAccountWager = $("#bonus_account_wager").html();
//                    return bonusAccountWager == undefined? "0" : bonusAccountWager.match(/(0\.)\w+/g)[0];
                    return bonusAccountWager == undefined? "0" : bonusAccountWager.replace(' BTC','');
                }
                
                
  }
})();

//Freebitcoin.sync();
var AutoRedeem = (function(){
  var showLog = false;
  var busy = false;
  var intervalTimer = 1000;

  var openRewardTab = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#rewards_tab').css('display') == "block"){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('click tab reward');
          $('.rewards_link')[0].click()
        }

      },intervalTimer);
    });
    return promise;
  }

  var showRewardPointBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#free_points_rewards').css('display') == "block"){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('show reward point bonus');
          $('#free_points_rewards').siblings('.reward_category_name').click();
        }

      },intervalTimer);
    });
    return promise;
  }


  var redeemRewardPointBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#bonus_span_free_points').html() != undefined){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('redeem reward point bonus');
          RedeemRPProduct('free_points_100')
        }

      },intervalTimer);
    });
    return promise;
  }

  //free btc
  var showFreeBTCBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#fp_bonus_rewards').css('display') == "block"){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('show free btc bonus');
          $('#fp_bonus_rewards').siblings('.reward_category_name').click();
        }

      },intervalTimer);
    });
    return promise;
  }


  var redeemFreeBTCBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#bonus_span_fp_bonus').html() != undefined){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('redeem free btc bonus');
          RedeemRPProduct('fp_bonus_100');
        }

      },intervalTimer);
    });
    return promise;
  }

  return{
    isBusy : function(){
      return busy;
    },
    isHaveBonusRewardPoint : function(){
      return !($('#bonus_span_free_points').html() == undefined);
    },
    isHaveBonusFreeCoin : function(){
      return !($('#bonus_span_fp_bonus').html() == undefined);
    },
    redeemBonusRewardPoint : function(){
      if(busy)return;
      busy = true;
      //reward point bonus
      Helper.waitRandom()
      //1. open reward tab
      .then(openRewardTab)
      //2. show reward point bonus
      .then(showRewardPointBonus)
      //3. redeem
      .then(redeemRewardPointBonus)
      ;
      busy = false;
    },
    redeemBonusFreeCoin : function(){
      if(busy)return;
      busy = true;
      //reward point bonus
      Helper.waitRandom()
      //1. open reward tab
      .then(openRewardTab)
      //2. show free btc bonus
      .then(showFreeBTCBonus)
      //3. redeem
      .then(redeemFreeBTCBonus)
      .then(function(){
        clearInterval(redeemTimer);
      })
      ;
      busy = false;
    },
    setLog : function(status){
      showLog = status;
    }
  }

})();

var AutoRoll = (function(){

  var showLog = false;
  // var isWaiting = false;
  var rollTimer = null;
  var busy = false;
  var intervalTimer = 1000;
  var isClick = false;

  var before = {
      rp : null,
      btc : null,
      percent : null
  };

  var waiting = function(){
    var countdownMinute = $('#time_remaining .countdown_amount').eq(0).html();
    if(showLog)console.log('countdownMinute : '+ countdownMinute);

    return (countdownMinute != undefined);
  }

  function getRollNumber(){
    var first = document.getElementById('multiplier_first_digit').innerHTML;
    var second = document.getElementById('multiplier_second_digit').innerHTML;
    var third = document.getElementById('multiplier_third_digit').innerHTML;
    var fourth = document.getElementById('multiplier_fourth_digit').innerHTML;
    var fifth = document.getElementById('multiplier_fifth_digit').innerHTML;
    var rollNumber = first+second+third+fourth+fifth;

    return rollNumber;
  }

  function doRoll(){
    var ret = new Promise(function(resolve, reject){
      var rollNumber = "";
        before.rp = Freebitcoin.getRewardPoint();
        before.btc = Freebitcoin.getBalance();
        before.percent = Freebitcoin.getBonusIncreasePercent();
        
        if(!isClick){
            isClick = true;
            $('#free_play_form_button').click();
            rollTimer = setInterval(function() {
                var newRollNumber = getRollNumber() ;
                if(newRollNumber === rollNumber){
                  clearInterval(rollTimer);
                  return resolve(newRollNumber);
                }
                else{
                  rollNumber = newRollNumber;
                }

            }, intervalTimer);
        }


    });
    return ret;
  }

    function saveLog(rollNumber){
        var promise = new Promise(function(resolve, reject){
                  
                  $.ajax({
                    method: "POST",
                    url: "http://127.0.0.1:3000/log_roll",
                    data: {
                        roll_number : rollNumber,
                        won_btc : Freebitcoin.getRollWinning(), 
                        won_rp : Freebitcoin.getRollRPWinning(), 
                        rp_before : before.rp, 
                        rp_after : Freebitcoin.getRewardPoint(), 
                        btc_before : before.btc, 
                        btc_after : Freebitcoin.getBalance(), 
                        bonus_inc_percent_before : before.percent, 
                        bonus_inc_percent_after : Freebitcoin.getBonusIncreasePercent()
                    }
                    })
                    .done(function( msg ) {
                      console.log("save log roll done");
                      resolve();
                    })
                    .error(function(request, status, error){
                        console.log("ajax error : "+error);
                    });
                  
          });
          return promise
    }

  return{
    isRollAvailable : function(){
      return !waiting();
    },
    doRoll : function(){

      Helper.waitRandom()
      .then(doRoll)
      .then(saveLog)
      
      ;
    },
    setLog : function(status){
      showLog = status;
    },
  }

})();

// autoRoll.start();

var stat = (function(){
  var counterWin = 0;
  var counterLose = 0;
  var counter = 0;
  var sumWager = 0;
  
  return{
    addWin : function(){
      counter++;
      counterWin++;
    },
    addLose : function(){
      counter++;
      counterLose++;
    },
    addWager : function(wager){
      sumWager+= wager;
    }, 
    show : function(){
      console.log('========================');
      console.log('sumWager : '+sumWager );
      console.log('counter : '+counter );
      console.log('counterWin : '+counterWin );
      console.log('counterLose : '+counterLose );
      console.log('winRate : '+ (counterWin/counter)*100 );
      console.log('========================');
    }
  }
})();
var trend = (function(){
  var HIGH = 1;
  var LOW = -1;
  var NO_STATE = 0;
  
  var rollResult = [];
  var roll = [];
        var lastRoll = 0;
  return{
                setLastRoll : function(number){
                    lastRoll = number;
                },
                getLastRoll : function (){
                    return lastRoll;
                },
    setInputResult : function(input){
      rollResult.push(input);
    },
    setInput : function(input){
      roll.push(input);
    },
    setInputResultHigh: function(){
      this.setInputResult(HIGH);
    },
    setInputHigh: function(){
      this.setInput(HIGH);
    },
    setInputResultLow: function(){
      this.setInputResult(LOW);
    },
    setInputLow: function(){
      this.setInput(LOW);
    },
    setInputResultNoState: function(){
      this.setInputResult(NO_STATE);
    },
    countLastTrend : function(num){
      var arrayLength = rollResult.length;
      if(arrayLength > num){
        var last = rollResult.slice((arrayLength-num), arrayLength);
        return this.countTrendChunk(last);
      }
      else{
        return this.countTrendChunk(rollResult);
      }
      
      
    },
    countTrend : function(){
      return this.countTrendChunk(rollResult);
    },
    countTrendChunk : function(rollResult){
        var countHigh = 0;
        var countLow = 0;
        var countOutState = 0;

        var countHighStreak = [];
        var counterHighStreak = 0;
        var statusHighStreak = false;

        var countLowStreak = [];
        var counterLowStreak = 0;
        var statusLowStreak = false;

        var alternateStreakBeginHigh = [];
        var alternateStreakBeginLow = [];

        var statusAlternateStreak = false;
        var counterAlternateStreak = 0;
        

        rollResult.forEach(function(item, index){
//          console.log('item : ',item);
//          console.log('index : ', index);
//          console.log('before : ', rollResult[index-1]);
//          console.log('---------------------');

          statusHighStreak = false;
          statusLowStreak = false;
          statusAlternateStreak = false;

          if(item == 1){
            countHigh++;
            statusHighStreak = true;

            if(rollResult[index-1] == -1){
              statusAlternateStreak = true;
            }
          }
          else if (item == -1){
            countLow++;
            statusLowStreak = true;

            if(rollResult[index-1] == 1){
              statusAlternateStreak = true;
            }

          }
          else{
            countOutState++;
          }

          //high streak count
          if(statusHighStreak == true){
            counterHighStreak++;
          }
          else if(statusHighStreak == false){
            if(counterHighStreak > 1){
              //save new record
              countHighStreak.push(counterHighStreak);
//              console.log('new high steak');

            }
            counterHighStreak = 0;

          }

          //low streak count
          if(statusLowStreak == true){
            counterLowStreak++;
          }
          else if(statusLowStreak == false){
            if(counterLowStreak > 1){
              //save new record
              countLowStreak.push(counterLowStreak);
//              console.log('new low steak');
            }
            counterLowStreak = 0;
          }

          //alternate streak count
          if(statusAlternateStreak == true){
            if(counterAlternateStreak == 0){
              counterAlternateStreak = 2;
            }
            else{
              counterAlternateStreak++;
            }
          }
          else if (statusAlternateStreak == false){
            if(counterAlternateStreak > 1){
//              console.log('check alternate');
//              console.log('counterAlternateStreak '+counterAlternateStreak);
//              console.log('first '+rollResult[index-counterAlternateStreak]);
//              console.log('index first '+(index-counterAlternateStreak));
              //alternate begin high
              if(rollResult[index-counterAlternateStreak] == 1){
                alternateStreakBeginHigh.push(counterAlternateStreak);
              }
              //alternate begin low
              else if(rollResult[index-counterAlternateStreak] == -1){
                alternateStreakBeginLow.push(counterAlternateStreak);
              }
            }

            counterAlternateStreak=0;
          }


          //condition last array
          if(index == (rollResult.length-1)){
//            console.log('last index');
            if(counterLowStreak > 1){
              //save new record
              countLowStreak.push(counterLowStreak);
              // console.log('new low steak');
            }
            if(counterHighStreak > 1){
              //save new record
              countHighStreak.push(counterHighStreak);
              // console.log('new high steak');
            }
            if(counterAlternateStreak > 1){
              //alternate begin high
              if(rollResult[index-counterAlternateStreak] == 1){
                alternateStreakBeginHigh.push(counterAlternateStreak);
              }
              //alternate begin low
              else if(rollResult[index-counterAlternateStreak] == -1){
                alternateStreakBeginLow.push(counterAlternateStreak);
              }
            }
          }



        });

        //print
//        console.log('roll :', roll);
//        console.log('rollResult :', rollResult);
//        console.log('countHigh : '+countHigh);
//        console.log('countLow : '+countLow);
//        console.log('countOutState : '+countOutState);

        console.log('countHighStreak : '+countHighStreak);
        console.log('countLowStreak : '+countLowStreak);
        console.log('alternateStreakBeginHigh : '+alternateStreakBeginHigh);
        console.log('alternateStreakBeginLow : '+alternateStreakBeginLow);
        
//        var MaxHighStreak =  Math.max(...countHighStreak);
//        var MaxLowStreak =  Math.max(...countLowStreak);
//        var MaxAlternateHighStreak =  Math.max(...alternateStreakBeginHigh);
//        var MaxAlternateLowStreak =  Math.max(...alternateStreakBeginLow);
        
        var getSum = function(total, num){
          return total+num;
        }
        
        var sumHighStreak =  countHighStreak.length == 0 ?0:countHighStreak.reduce(getSum);
        var sumLowStreak =  countLowStreak.length == 0 ? 0: countLowStreak.reduce(getSum);
        var sumAlternateHighStreak =  alternateStreakBeginHigh.length == 0?0:alternateStreakBeginHigh.reduce(getSum);
        var sumAlternateLowStreak =  alternateStreakBeginLow.length == 0?0:alternateStreakBeginLow.reduce(getSum);
        
        console.log('sumHighStreak : ',sumHighStreak);
        console.log('sumLowStreak : ',sumLowStreak);
        console.log('sumAlternateHighStreak : ',sumAlternateHighStreak);
        console.log('sumAlternateLowStreak : ',sumAlternateLowStreak);
        
        var tempResult = [
          sumHighStreak,
          sumLowStreak,
          sumAlternateHighStreak,
          sumAlternateLowStreak
        ];
        
        maxResultValue = Math.max(...tempResult);
        
        return tempResult.indexOf(maxResultValue);
      }
  }
})();

var manualBet = (function(){
  const intervalTimer = 1000;
  var showLog = false;



  function getRollNumber(){
    var first = document.getElementById('multiplier_first_digit').innerHTML;
    var second = document.getElementById('multiplier_second_digit').innerHTML;
    var third = document.getElementById('multiplier_third_digit').innerHTML;
    var fourth = document.getElementById('multiplier_fourth_digit').innerHTML;
    var fifth = document.getElementById('multiplier_fifth_digit').innerHTML;
    var rollNumber = first+second+third+fourth+fifth;

    return rollNumber;
  }

  function isRollingDone(){
    isRollingDone.newNumber = getRollNumber();
    var ret;
    ret = isRollingDone.newNumber == isRollingDone.oldNumber;
    isRollingDone.oldNumber = isRollingDone.newNumber;
    return ret;

  };





  return{
    
  setTrend : function(rollNumber){
        trend.setLastRoll(rollNumber);
   if(rollNumber < 4750){
     trend.setInputResultLow();
   }
   else if (rollNumber > 5250){
     trend.setInputResultHigh();
   }
   else{
     trend.setInputResultNoState();
   }
    
  },
  betLow : function(){
      var clickDone = false;
      var _this = this;
      var promise = new Promise(function(resolve, reject){
        var timer = setInterval(function(){
          if(isRollingDone()&&clickDone){
            clearInterval(timer);
            var rollNumber = getRollNumber(); 
            var ret = (rollNumber < 4750);
            if(showLog)console.log('bet result : '+ret);
            
            trend.setInputLow();
            _this.setTrend(rollNumber);
            
            var retBet = {
                type : 'LOW',
                result : ret
            };
            
            return resolve(retBet);
          }
          else{

            if(clickDone){
              if(showLog)console.log('waiting roll');
            }
            else{
                if(showLog)console.log('betLow');
                $('#double_your_btc_bet_lo_button').click();
                clickDone = true;
            }

          }

        },intervalTimer);
      });
      return promise;
    },
    betHigh : function(){
      var clickDone = false;
      var _this = this;
        var promise = new Promise(function(resolve, reject){
          var timer = setInterval(function(){
            if(isRollingDone()&&clickDone){
              clearInterval(timer);
              var rollNumber = getRollNumber(); 
              
              
              var ret = ( rollNumber > 5250);
              if(showLog)console.log('bet result : '+ret);
              
              trend.setInputHigh();
              _this.setTrend(rollNumber);
              
              
            var retBet = {
              type : 'HIGH',
              result : ret
            };
              
              return resolve(retBet);
            }
            else{

              if(clickDone){
                if(showLog)console.log('waiting roll');
              }
              else{
                  if(showLog)console.log('betHigh');
                  $('#double_your_btc_bet_hi_button').click();
                  clickDone = true;
              }

            }

          },intervalTimer);
        });
        return promise;
    },
    
     setBetAmount : function(amount){
       var promise = new Promise(function(resolve, reject){
         var timer = setInterval(function(){
           if($('#double_your_btc_stake').val() == amount){
             clearInterval(timer);
             return resolve();
           }
           else{
             if(showLog)console.log('set bet amount : '+amount);
             $('#double_your_btc_stake').val(amount);
           }

         },intervalTimer);
       });
       return promise;
     },
    // openBetTab = openBetTab()
    //open multiply BTC tab
    openBetTab : function(){
      var promise = new Promise(function(resolve, reject){
        var timer = setInterval(function(){
          if($('#double_your_btc_tab').css('display') == "block"){
            clearInterval(timer);
            return resolve();
          }
          else{
            if(showLog)console.log('click tab bet');
            $('.top-bar-section .double_your_btc_link').click()
          }

        },intervalTimer);
      });
      return promise;
    }
  }
})();

var sequenceBet = (function(){
  /*
   * mode
   * high : 0
   * low : 1
   * alternateHigh : 2
   * alternateLow : 3
   */
  var MODE_HIGH = 0;
  var MODE_LOW = 1;
  var MODE_ALTERNATE_HIGH = 2;
  var MODE_ALTERNATE_LOW = 3;
        var MODE_ALTERNATE_NOW = 4;
  var showLog = false;
        
        var TRANSLATE_MODE = ['MODE_HIGH', 'MODE_LOW', 'MODE_ALTERNATE_HIGH', 'MODE_ALTERNATE_LOW', 'MODE_ALTERNATE_NOW'];
  
  var currentMode = null;
  var counter = 1;
  return {
    setModeHigh : function(){
      this.setMode(MODE_HIGH);
    },
    setModeLow : function(){
      this.setMode(MODE_LOW);
    },
    setModeAlternateHigh : function(){
      this.setMode(MODE_ALTERNATE_HIGH);
    },
    setModeAlternateLow : function(){
      this.setMode(MODE_ALTERNATE_LOW);
    },
                setModeAlternateNow : function(){
      this.setMode(MODE_ALTERNATE_NOW);
    },
    setMode : function(mode){
      if(showLog)console.log('set mode ', mode);
      if(currentMode == mode)return;
      currentMode = mode;
      counter = 1;
    },
    getMode : function(){
      return currentMode;
    },
                getModeText : function(){
                    return TRANSLATE_MODE[currentMode];
                },
                inverseBet : function(){
                    //mode alternate now
                    if(currentMode == MODE_ALTERNATE_NOW){
                        if(trend.getLastRoll() < 5000){
                            return manualBet.betHigh();
                        }
                        else{
                            return manualBet.betLow();
                        }
                    }
                    else{
                        console.log('inverseBet error');
                    }
                },
    nextBet : function(isPrevWin){
                        
                    
                    
                        if(isPrevWin){
                            counter++;
                        }
                        else{
                            counter = 1;
                        }
      
      var isOdd = (counter % 2) == 1;
      
      if(currentMode == MODE_HIGH){
        if(showLog)console.log('bet high');
        return manualBet.betHigh();
      }
      else if(currentMode == MODE_LOW){
        if(showLog)console.log('bet low');
        return manualBet.betLow();
      }
      else if(currentMode == MODE_ALTERNATE_HIGH){
                                if(!isPrevWin){
                                    if(showLog)console.log('bet high');
                                    return manualBet.betHigh();
                                }
        
                                if(isOdd){
          if(showLog)console.log('bet high');
          return manualBet.betHigh();
        }
        else{
          if(showLog)console.log('bet low');
          return manualBet.betLow();
        }
                                
        
      }
      else if (currentMode == MODE_ALTERNATE_LOW){
                                if(!isPrevWin){
                                    if(showLog)console.log('bet low');
                                    return manualBet.betLow();
                                }
                                
        if(isOdd){
          if(showLog)console.log('bet low');
          return manualBet.betLow();
        }
        else{
          if(showLog)console.log('bet high');
          return manualBet.betHigh();
        }
      }
      else{
        if(showLog)console.log('undefined mode');
      }
      
    }
  }
})();
var Labourchere = (function(){
  var betList = null;
  var showLog = false;
  var counterSwitch = 0;
  var counterBet = 0;
  var tempTrend = 0;

  var accountBalanceBefore = 0;
  var accountBalance = 0;
  var accountBalanceAfter = 0;
  var self = this;

  var prevResult = true;
  var prevBTC = null;
  var prevRP = null;
  var betBatch = null;
  
  var timer = null;
  
  var stop = function(){
      clearInterval(timer);
  };

  var init = function(){
      counterBet=0;
      if(localStorage.getItem('betList') == null || localStorage.getItem('betList') == 'null'){
        betList = [];
        for(var i=0;i<10;i++){
            betList.push("0.00000001");
        }
      }
      else{
        betList = JSON.parse(localStorage.getItem('betList'));
      }

      if((localStorage.getItem('betBatch') == null) || (localStorage.getItem('betBatch') == 'null')){
          localStorage.setItem('betBatch', 1);
          betBatch = 1;
      }
      else{
          betBatch = localStorage.getItem('betBatch');
          betBatch++;
          localStorage.setItem('betBatch', betBatch);
      }
      
      sequenceBet.setModeAlternateNow();
  };

  var start = function(){
      
    var waiting = false;
    init();
 
    accountBalanceBefore = Freebitcoin.getBalance();
    accountBalanceBefore = parseFloat(accountBalanceBefore);
    accountBalance = accountBalanceBefore;
    
    var percentBefore = Freebitcoin.getBonusIncreasePercent();
    var bonusBalanceBefore = Freebitcoin.getBonusBalance();
    var bonusWagerRemainingBefore = Freebitcoin.getBonusWagerRemaining();
    
    
    
    timer = setInterval(function(){
      if(waiting)return;
      
//      if(counterSwitch >= 13){
//        counterSwitch = 0
//        tempTrend = trend.countLastTrend(21);
//        if(showLog)console.log('get trend', tempTrend);
//      }
//      else{
//        counterSwitch++;
//        if(showLog)console.log('counter switch', counterSwitch);
//      }
      
//      switch(tempTrend){
//        case 0 : sequenceBet.setModeHigh(); break;
//        case 1 : sequenceBet.setModeLow(); break;
//        case 2 : sequenceBet.setModeAlternateHigh(); break;
//        case 3 : sequenceBet.setModeAlternateLow(); break;
//        default : sequenceBet.setModeLow();break;
//      }
      
      if(betList.length == 0){
        if(showLog)console.log('=== batch done ===');
        localStorage.removeItem('betList');
//        accountBalanceAfter = accountBalance;
//        if(showLog)console.log('accountBalanceAfter : '+ accountBalanceAfter.toFixed(8).toString());
        clearInterval(timer);
        waiting = false;
//        trend.countTrend();
        Bot.resetBet();
      }
      else{
        waiting = true;
        var betAmount = null;
        if(betList.length == 1){
            betAmount = betList[0];
        }
        else{
            betAmount = parseFloat(betList[0]) + parseFloat(betList[betList.length-1]);
            betAmount = betAmount.toFixed(8).toString();
        }
        
        //checking balance before bet.
        if(parseFloat(betAmount) > accountBalance){
            console.log('no money no cry');
            
                  
                  $.ajax({
                    method: "POST",
                    url: "http://127.0.0.1:3001/alert",
                    data: {
                        btc : Freebitcoin.getBalance(), 
                        counter : counterBet
                    }
                    });
                    
                
            return;
        }
        
        accountBalance -= parseFloat(betAmount);
        prevBTC = Freebitcoin.getBalance();
        prevRP = Freebitcoin.getRewardPoint();

        
        

        if(showLog)console.log('bet amount : '+betAmount);
        stat.addWager(parseFloat(betAmount));
        
        manualBet.openBetTab()
        .then(manualBet.setBetAmount(betAmount))
        //set
        .then(function(){
//          return sequenceBet.nextBet(prevResult);
          return sequenceBet.inverseBet();
        })
        
        .then(function(betRet){
            var isWin = betRet.result;
            prevResult = isWin;
          if(showLog)console.log('bet resutl :'+isWin);
          
          if(isWin == undefined){
//            console.log(isWin);
            console.log('system error');
            console.log(isWin);
            return;
          }
          
         
          
          var promise = new Promise(function(resolve, reject){
               if(isWin){
                    stat.addWin();
                    betList.shift();
                    betList.pop();
                    accountBalance += (parseFloat(betAmount)*2)
                  }
                  else{
                    stat.addLose();
                    betList.push(betAmount);
                  }
                  if(showLog)console.log('list : '+ betList);
                  if(showLog)console.log('list.length : '+ betList.length);
                  if(showLog)console.log('accountBalanceBefore : '+ accountBalanceBefore.toFixed(8).toString());
                  if(showLog)console.log('accountBalance : '+ accountBalance.toFixed(8).toString());
                  
                  localStorage.setItem('betList', JSON.stringify(betList));
                  
                  $.ajax({
                    method: "POST",
                    url: "http://127.0.0.1:3000/log_bet",
                    data: {
                        batch : betBatch,
                        bet_amount: parseFloat(betAmount), 
                        bet_choice: betRet.type, 
                        roll_number: Freebitcoin.getBetRollNumber(), 
                        is_win: isWin?1:0,
                        btc_before: prevBTC, 
                        btc_after: Freebitcoin.getBalance(), 
                        rp_before: prevRP, 
                        rp_after: Freebitcoin.getRewardPoint(),
                        bonus_inc_percent_before : percentBefore,
                        bonus_inc_percent_after : Freebitcoin.getBonusIncreasePercent(),
                        min_reward : Freebitcoin.getMinReward(),
                        bonus_balance_before : bonusBalanceBefore,
                        bonus_balance_after : Freebitcoin.getBonusBalance(),
                        bonus_wager_remaining_before : bonusWagerRemainingBefore,
                        bonus_wager_remaining_after : Freebitcoin.getBonusWagerRemaining(),
                        bet_strategy : sequenceBet.getModeText()
                    }
                    })
                    .done(function( msg ) {
                      waiting = false;
                      console.log("save done");
                      counterBet++;
                      resolve();
                    })
                    .error(function(request, status, error){
                        console.log("ajax error : "+error);
                        stop();
                    });
                  
          });
          return promise
        })
        .then(function(){
            console.log("stat");
            stat.show();
        });
        ;
      }
      }, 3000);
  }

  return{
    start : start,
    setLog : function(status){
      showLog = status;
    },
    stop : stop,
    cron : function(){
      var done = false;

      var timer = setInterval(function(){
        var countdownMinute = $('#time_remaining .countdown_amount').eq(0).html();
        if(showLog)console.log('countdownMinute : '+ countdownMinute);
        if ((countdownMinute != undefined)&&(countdownMinute < 50)){
          start();

          done = true;
          clearInterval(timer);
        }
      }, 5000);


    }
  }



})();

//labourchere.cron();

var Bot = (function(){
  const intervalTimer = 5000;
  var timer = null;
  var PROD = 1;
  var DEV = 0;
  var env = PROD;
  var betCount = 0;
  
  var sleepHours = [11,12];
  var alarmTimer = null;
  var date = null;
  
  var start = function(){
        date = new Date();
        console.log("Bot Started");
        var currentHour = date.getHours();

        if(sleepHours.indexOf(currentHour) != -1){
            alarmTimer = setInterval(function(){
            
            date = new Date();
            currentHour = date.getHours();
            
            if(sleepHours.indexOf(currentHour) == -1){
                clearInterval(alarmTimer);
                location.reload(true);
            }
                
            }, intervalTimer);
            return;
        }
      
        if(!AutoRedeem.isHaveBonusRewardPoint() && (env == PROD)){
                AutoRedeem.redeemBonusRewardPoint();
                timer = setTimeout(start, intervalTimer);
        }
  //      else
//        if(!AutoRedeem.isHaveBonusFreeCoin() && (env == PROD)){
//              AutoRedeem.redeemBonusFreeCoin();
 //             timer = setTimeout(start, intervalTimer);
 //       }
        else
        //if(AutoRoll.isRollAvailable() && AutoRedeem.isHaveBonusFreeCoin() && AutoRedeem.isHaveBonusRewardPoint() && (env == PROD)){
        if(AutoRoll.isRollAvailable() && AutoRedeem.isHaveBonusRewardPoint() && (env == PROD)){    
                AutoRoll.doRoll();
                timer = setTimeout(start, intervalTimer);
        }
        else{
              if(betCount == 0){
                betCount = 1;
                console.log("Labourchere start");
                //Labourchere.start();  
              }

        }
  }
  
  return {
    start : function(){
//      AutoRedeem.setLog(true);
//      AutoRoll.setLog(true);
//      Helper.setLog(true);
        
        

      timer = setTimeout(start, intervalTimer);
    },
    stop : function(){
      clearTimeout(timer);
    },
    resetBet : function(){
        betCount = 0;
        this.start();
    }
  }
})();

window.onload = function() {
  Bot.start();
};
})();