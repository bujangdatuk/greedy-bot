class History{
    constructor(){
       if(typeof History.instance === 'object'){
           return History.instance;
       }
      this._record = [];
       History.instance = this;
       return this;
    }
    reset(){
        this._record = [];
    }
    add(record){
      // if (record.hasOwnProperty('betAmount') && record.hasOwnProperty('isWin') && record.hasOwnProperty('betType') && record.hasOwnProperty('rollNumber'))
        this._record.push(record);
    }
    getLastRecord(){
        return this._record[this._record.length-1];
    }
    get record(){
        return this._record;
    }
    
}