class AOPLog{
	constructor(){

	}
	showDetails(fn){
		return function(){
			console.log("function "+fn.name);
			var paramsFn = ""
			if(arguments.length > 0){
				for (let i=0;i<arguments.length;i++){
					paramsFn += arguments[i] + " ";
				}
				
			}
			console.log("params :"+ paramsFn);

			var ret = fn.apply(this, arguments);

			
			if(Promise.resolve(ret) == ret){
				ret.then(function(retValueProm){
					console.log("return :"+ retValueProm);
				});
			}
			else{
				console.log("return :"+ ret);
			}
			

			return ret;
		}
	}
}

var aopLog = new AOPLog();


// apply trend
FreeBitCoin.prototype.bet = aopLog.showDetails(FreeBitCoin.prototype.bet);