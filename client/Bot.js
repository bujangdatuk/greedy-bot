
class Bot{
    constructor(bettingStrategy, decisionRule, bettingSite){
        this._bettingStrategy = bettingStrategy;
        this._decisionRule = decisionRule;
        this._bettingSite = bettingSite;
        this._timer = null;
        this._onProcess = false;
        this._history = new History();
        this._statistic = new Statistic(this._history);
        this._counterRepeat = 0;
        this._repeat = 1;
    }
    
    init(){

    }
    
    start(){
        if(arguments.length == 1){
            this._repeat = arguments[0];
        }


        var self = this;

        this._bettingStrategy.initBetList();
        this._timer = setInterval(function(){

            
            if(self._onProcess)return;

            if(self._bettingSite.getRefreshTimeRemaining() < 5){
                console.log("wait till next batch");
                self.stop();
                return;
            }


            var betAmount = self._bettingStrategy.getBetAmount();
            var balanceBefore = self._bettingSite.getBalance();
            var balanceAfter = null;
            
            self._onProcess = true;
            
            self._bettingSite.setBetAmount(betAmount).then(function(){
                var betType = self._decisionRule.bet();
                self._bettingSite.bet(betType).then(function(result){
                    var isWin = false;
                    if (betType == Bet.HIGH && result == RollType.HIGH) isWin = true;
                    if (betType == Bet.LOW && result == RollType.LOW) isWin = true;
                    

                    self._history.add({
                        betAmount : betAmount,
                        isWin : isWin,
                        betType : betType,
                        rollType : result
                    });

                    //add bet validation
                    balanceAfter = self._bettingSite.getBalance();
                    var isFair = self._bettingStrategy.validateBet(balanceBefore, betAmount, isWin, balanceAfter);
                    if(!isFair){
                        console.log("not fair, please stop!");
                        self.stop();
                        return;
                    }

                    self._bettingStrategy.evaluateResult(isWin);
                    self._onProcess = false;
                    if(self._bettingStrategy.isDone()){
                        self._counterRepeat++;
                        self.stop();
                        self._statistic.show();
                        self._history.reset();
                    }
                });
                
            })
        },2000);
        
            
        
    }
    stop(){
        clearInterval(this._timer);
        if(this._counterRepeat < this._repeat){
            this.start();
        }
    }
}