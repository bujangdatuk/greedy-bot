class BettingStrategy{
	constructor(){

	}
	validateBet(balanceBefore, betAmount, betResult, balanceAfter){
		balanceBefore = parseFloat(balanceBefore).toFixed(8);
		balanceAfter = parseFloat(balanceAfter).toFixed(8);
		betAmount = parseFloat(betAmount).toFixed(8);

        if(!betResult){
        	betAmount *= -1;
        } 

        var expected = parseFloat(balanceBefore) + parseFloat(betAmount);
        expected = parseFloat(expected).toFixed(8);

        return (balanceAfter == expected);
    }
}