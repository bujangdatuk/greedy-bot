class Labouchere extends BettingStrategy{
    constructor(targetWin){
        super();
        this._targetWin = targetWin;
//        this._betList = [];
        
        // this._history = new History();
        this._betList = new ArrayLS("labouchere");
        this.initBetList();

        
    }
    initBetList(){
        var targetWin = this._targetWin;
        
        if(! this._betList.isEmpty())return;
        
        var itemList = parseFloat((targetWin/10).toFixed(8)).toFixed(8);
        for(let i=0;i<10;i++){
            this._betList.push(itemList);
        }
    }

    isDone(){
        return this._betList.length == 0;
    }
    getBetAmount(){
        var betList = this._betList.copy();
        
        
        if(betList.length == 0){
            return  0;
        }
        
        if(betList.length == 1){
            return  betList[0];
        }
            
        var firstElement = betList[0];
        var lastElement = betList[betList.length-1];
        var betAmount = parseFloat(firstElement) + parseFloat(lastElement);
        
        return parseFloat(betAmount).toFixed(8);
    }
    
    evaluateResult(isWin){

        
        var betAmount = this.getBetAmount();
        
        if(betAmount == 0){
            this._betList.clear();
            return;
        }
        
        
        // this._history.add({
        //     betAmount,
        //     isWin
        // });
        
        if(isWin){
            this._betList.pop();
            this._betList.shift();
        }
        else{
            this._betList.push(betAmount);
        }
        
        
    }
    
    get betList(){
        return this._betList.copy();
    }
    
    set betList(betList){
//        this._betList = betList;
        this._betList.assign(betList);
    }
    
    // get history(){
    //     return this._history;
    // }
}