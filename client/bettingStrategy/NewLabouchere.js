class NewLabouchere extends Labouchere{
    constructor(){
        let targetWin, maxBet, increaseMaxBet;
        [targetWin = 0.00000010, maxBet=0.00001000, increaseMaxBet = false] = arguments;
        
        
        super(targetWin);
        this._maxBet = maxBet;
        this._tmpMaxBet = maxBet;
//        this._childBetList = [];
        this._childBetList = new ArrayLS('newlabouchere');
        this._increaseMaxBet = increaseMaxBet;
        
    }
    
    get maxBet(){
        return this._maxBet;
    }
    
    multipleMaxbet(){
        if(this._increaseMaxBet)this._maxBet += this._tmpMaxBet;
    }
    
    evaluateResult(isWin){
        super.evaluateResult(isWin);
        
        var betAmount = super.getBetAmount();
        
        if(betAmount >= this._maxBet){
//            console.log('amount : '+betAmount);
//            console.log('length : '+super.betList.length);
            
            if(super.betList.length > 2){
//                console.log('shift');
                super.betList.shift();
            }
//            console.log('pop');
            super.betList.pop();
            
            this._childBetList.push(super.betList);
            
            
            
            var item = parseFloat((betAmount/10).toFixed(8)).toFixed(8);
            var tmpList = [];
            
            for(let i=0;i<10;i++){
                tmpList.push(item);
            }
            
            this._childBetList.push(tmpList);
            
            super.betList = this._childBetList.pop();
            
            this.multipleMaxbet();
        }
        else if (betAmount == 0){
            if(this._childBetList.length > 0){
                super.betList = this._childBetList.pop();
            }
        }
        

        
        
        
        
    }
}